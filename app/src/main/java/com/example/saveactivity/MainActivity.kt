package com.example.saveactivity

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var sharedPreference: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init() {
        sharedPreference = getSharedPreferences("info", MODE_PRIVATE)
    }
    fun save (view:View){
        val email = emailET.text.toString()
        val firstName = nameET.text.toString()
        val lastName = lastnameET.text.toString()
        val age = ageET.text.toString()
        val address = addressET.text.toString()

        val editor = sharedPreference.edit()
        editor.putString("email",email)
        editor.putString("name",firstName)
        editor.putString("lastname",lastName)
        editor.putString("age",age)
        editor.putString("email",address)
        editor.apply()

        val email = sharedPreference.getString("email", "")
        val name = sharedPreference.getString("firstname", "")
        val surename = sharedPreference.getString("lastname", "")
        val address = sharedPreference.getString("age", "")
        val age = sharedPreference.getString("address", "")
        emailET.setText(email)
        nameET.setText(firstName)
        lastnameET.setText(lastName)
        addressET.setText(age)
        ageET.setText(address)
    }
    fun delete(view: View){
        val editor = sharedPreference.edit()
        editor.clear()
        editor.apply()
    }
}